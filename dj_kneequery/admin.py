from django.contrib import admin
from .models import *


admin.site.register(Patient)
admin.site.register(Antropometry)
admin.site.register(Womac)
admin.site.register(KSS)
admin.site.register(JK)
admin.site.register(Operation)
admin.site.register(Examination)
admin.site.register(TranexamAmount)
admin.site.register(TranexamHb)
admin.site.register(RupivacainPain)
admin.site.register(RupivacainAmount)
