from django.db import models
from django.db.models.signals import post_save
from django.db import connection
from .scales import *
from .sql import *


class Patient(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    name = models.CharField(
            max_length=100,
            default=None,
            blank=True,
            null=True,
            )
    age = models.IntegerField(default=None, blank=True, null=True)
    address = models.CharField(
            max_length=150,
            default=None,
            blank=True,
            null=True,
            )
    number = models.CharField(
            max_length=20,
            default=None,
            blank=True,
            null=True,
            )

    @property
    def operations(self):
        try:
            return self.operation_set.all()
        except Exception:
            return None

    @property
    def is_in_queue(self):
        try:
            print(Queue.objects.filter(patient=self.id).get())
            return Queue.objects.filter(patient=self.id).get()
        except Exception:
            return None

    @property
    def position_in_queue(self):
        with connection.cursor() as c:
            c.execute(get_queue)
            record = next(
                    filter(
                            lambda k: k[1]['id'] == self.id,
                            enumerate(
                                    dictfetchall(c),
                                    start=1
                            )
                    )
            )
        return record[0]

    def __str__(self):
        return self.name


class Examination(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    patient = models.ForeignKey('Patient', on_delete=models.CASCADE)
    date = models.DateField(default=None, blank=True, null=True)
    joint = models.CharField(
            max_length=1,
            choices=(("l", "Левый"), ('r', "Правый")),
            default='',
            )
    arthrosis_stage = models.IntegerField(
            choices=((1, 1), (2, 2), (3, 3), (4, 4)),
            default=0,
            )
    treatment = models.CharField(
            max_length=50,
            default=None,
            blank=True,
            null=True,
            )
    examination = models.CharField(
            max_length=25,
            default=None,
            blank=True,
            null=True,
            )

    def __str__(self):
        return str(self.date)

    @property
    def womac(self):
        try:
            return self.womac_set.last()
        except Exception:
            return None

    @property
    def kss(self):
        try:
            return self.kss_set.last()
        except Exception:
            return None

    @property
    def antropometry(self):
        try:
            return self.antropometry_set.last()
        except Exception:
            return None

    @property
    def jk(self):
        try:
            return self.jk_set.last()
        except Exception:
            return None


class Antropometry(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    examination = models.ForeignKey(
            'Examination',
            on_delete=models.CASCADE,
            default=None,
            blank=True,
            null=True,
            )
    height = models.IntegerField(default=None, blank=True, null=True)
    weight = models.IntegerField(default=None, blank=True, null=True)
    bmi = models.FloatField(default=None, blank=True, null=True)

    def save(self, *args, **kwargs):
        try:
            self.bmi = self.weight * 10_000 / (self.height ** 2)
        except Exception:
            self.bmi = 0
        super().save(*args, **kwargs)


class Operation(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    patient = models.ForeignKey('Patient', on_delete=models.CASCADE)
    date = models.DateField(default=None, blank=True, null=True)
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=1000)


class Queue(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    patient = models.ForeignKey('Patient', on_delete=models.CASCADE)


class RupivacainAmount(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    operation = models.ForeignKey('Operation', on_delete=models.CASCADE)
    amount = models.FloatField(default=None, blank=True, null=True)


class RupivacainPain(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    amount = models.ForeignKey('RupivacainAmount', on_delete=models.CASCADE)
    lag = models.IntegerField(default=None, blank=True, null=True)
    vas = models.IntegerField(
            choices=tuple(((i, str(i)) for i in range(1, 11))),
            default=1,
            help_text='Pain intensivity',
    )

    class Meta:
        ordering = ['lag']


class TranexamAmount(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    operation = models.ForeignKey('Operation', on_delete=models.CASCADE)
    amount = models.IntegerField(
            choices=((1, '1.0'), (2, '2.0'), ),
            default=1,
            help_text='Tranexam amount',
    )


class TranexamHb(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    amount = models.ForeignKey(
            'TranexamAmount',
            on_delete=models.CASCADE,
            default=None,
            blank=True,
            null=True,
    )
    lag = models.IntegerField(default=None, blank=True, null=True)
    hb = models.IntegerField(default=0, blank=True, null=True)

    class Meta:
        ordering = ['lag']


def save(self, *args, **kwargs):
    self.score = sum(
            (self.__dict__[str(i)] for i in range(1, len(kss_questions)))) / 2
    super(KSS, self).save(*args, **kwargs)


kss_dict = {str(i): models.IntegerField(
        choices=kss_questions[k],
        default=kss_questions[k][0][0],
        help_text=k,
        )
    for i, k in enumerate(kss_questions, start=1)
}
kss_dict['category'] = models.CharField(
        max_length=1,
        choices=kss_category,
        help_text='Категория пациента',
        default=kss_category[0][0],
        )
kss_dict['creation_date'] = models.DateTimeField(auto_now_add=True)
kss_dict['examination'] = models.ForeignKey(
        'Examination',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        default=None,
        )
kss_dict['score'] = models.IntegerField(default=None, blank=True, null=True)
kss_dict['save'] = save
kss_dict['__module__'] = Patient.__module__

KSS = type('KSS', (models.Model,), kss_dict)


def save(self, *args, **kwargs):
    self.score = sum(
            (self.__dict__[str(i)] for i in range(1, len(womac_questions))))
    super(Womac, self).save(*args, **kwargs)


womac_dict = {str(i): models.IntegerField(choices=(
        (0, 'нет'),
        (1, 'легко'),
        (2, 'умеренно'),
        (3, 'выражено'),
        (4, 'очень сильно')
    ), default=0, help_text=q) for i, q in enumerate(womac_questions, start=1)}
womac_dict['creation_date'] = models.DateTimeField(auto_now_add=True)
womac_dict['examination'] = models.ForeignKey(
        'Examination',
        on_delete=models.CASCADE,
        )
womac_dict['score'] = models.IntegerField(default=None, blank=True, null=True)
womac_dict['save'] = save
womac_dict['__module__'] = Patient.__module__

Womac = type('Womac', (models.Model,), womac_dict)


def save(self, *args, **kwargs):
    self.score = sum(
            (self.__dict__[str(i)] for i in range(1, len(jk_questions))))
    super(JK, self).save(*args, **kwargs)


jk_dict = {str(i): models.IntegerField(
        choices=jk_questions[q],
        default=jk_questions[q][0][0],
        help_text=q,
        )
    for i, q in enumerate(jk_questions, start=1)}
jk_dict['creation_date'] = models.DateTimeField(auto_now_add=True)
jk_dict['examination'] = models.ForeignKey(
        'Examination',
        on_delete=models.CASCADE,
        )
jk_dict['score'] = models.IntegerField(default=None, blank=True, null=True)
jk_dict['save'] = save
jk_dict['__module__'] = Patient.__module__

JK = type('JK', (models.Model,), jk_dict)


def create_scales_on_examination(instance, created, **kwargs):
    if created:
        Womac.objects.create(examination=instance)
        KSS.objects.create(examination=instance)
        JK.objects.create(examination=instance)
        Antropometry.objects.create(examination=instance)


def create_measurements_on_operation(instance, created, **kwargs):
    if created:
        TranexamAmount.objects.create(operation=instance)
        RupivacainAmount.objects.create(operation=instance)


post_save.connect(create_scales_on_examination, sender=Examination)
post_save.connect(create_measurements_on_operation, sender=Operation)
