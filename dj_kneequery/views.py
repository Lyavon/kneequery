from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import DetailView
from .models import *
from .sql import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import connection


class DetailedPatients(LoginRequiredMixin, DetailView):
    model = Patient
    template_name = 'detailed_patients.html'

    def get_object(self):
        pk = self.kwargs['pk']
        patient = Patient.objects.get(pk=pk)
        return patient


def ordering(request):
    'order by SQL part from provided request'
    ordering = request['order']
    ret = ''
    for rule in filter(
            lambda x: any((i == x for i in ('-name', 'name'))),
            ordering):
        ret += rule
        ret += ' '
        if rule.startswith('-'):
            ret += 'desc'
            ret += ' '


@login_required
def patients(request):
    asc_filters = (
        'name',
        'age',
        'operated',
        'womac', 'KSS',
        'jk',
        'category',
    )
    desc_filters = tuple(map(lambda x: '-' + x, asc_filters))

    def filter_valid(input):
        return list(filter(
                lambda x: any((i == x for i in asc_filters + desc_filters)),
                input))

    def make_unique(ordering):
        ret = []
        for name in ordering:
            if name in ret or name.split('-')[-1] in ret or ('-'+name) in ret:
                continue
            ret.append(name)
        return ret

    def generate_sql(ordering):
        rules = [
            (rule[1:] + ' desc') if rule.startswith('-') else rule
            for rule in ordering]
        return 'order by ' + ', '.join(rules)

    def template_filters(new_rule):
        return {('filter_' + asc): asc if desc in new_rule else desc
                for asc, desc in zip(asc_filters, desc_filters)}

    old_ordering = filter_valid(request.session.get('order', ['name']))
    new_ordering = filter_valid([request.GET.copy().get('order', 'name')])
    new_ordering += old_ordering
    new_ordering = make_unique(new_ordering)
    request.session['order'] = new_ordering

    with connection.cursor() as c:
        c.execute(get_patients + generate_sql(new_ordering))
        records = dictfetchall(c)

    context = template_filters(new_ordering)
    context['object_list'] = records
    return render(request, 'patients.html', context)


@login_required
def create_operation(request, pk):
    operation = Operation.objects.create(
            patient=Patient.objects.filter(id=pk).get())
    operation.save()
    return redirect('admin:dj_kneequery_operation_change', operation.id)


@login_required
def create_examination(request, pk):
    examination = Examination.objects.create(
            patient=Patient.objects.filter(id=pk).get()
    )
    examination.save()
    return redirect('admin:dj_kneequery_examination_change', examination.id)


@login_required
def create_rupivacainpain(request, pk):
    pain = RupivacainPain.objects.create(
            amount=RupivacainAmount.objects.filter(id=pk).get()
    )
    pain.save()
    return redirect('admin:dj_kneequery_rupivacainpain_change', pain.id)


@login_required
def create_tranexamhb(request, pk):
    hb = TranexamHb.objects.create(
            amount=TranexamAmount.objects.filter(id=pk).get()
    )
    hb.save()
    return redirect('admin:dj_kneequery_tranexamhb_change', hb.id)


@login_required
def queue(request):
    with connection.cursor() as c:
        c.execute(get_queue)
        records = dictfetchall(c)
    return render(request, 'queue.html', {'object_list': records})


@login_required
def add_to_queue(request, pk):
    position = Queue.objects.create(
            patient=Patient.objects.filter(id=pk).get()
    )
    position.save()
    return redirect('queue')


@login_required
def delete_from_queue(request, pk):
    Queue.objects.filter(patient=Patient.objects.filter(id=pk).get()).delete()
    return redirect('queue')


@login_required
def search(request):
    with connection.cursor() as c:
        c.execute(search_patient, ["%"+request.GET['query']+"%"])
        records = dictfetchall(c)
    return render(request, 'search.html', {'object_list': records})
