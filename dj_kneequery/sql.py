def dictfetchall(cursor):
    "Returns all rows from a cursor as a dict"
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]


get_queue = '''
    select
        p.id as id,
        p.name as name,
        age,
        womac,
        kss,
        category
    from
        dj_kneequery_queue as q
        left join dj_kneequery_patient as p on p.id = q.patient_id
        left outer join
        (
            select
                e.patient_id as pid,
                any_value(w.score) as womac,
                any_value(k.score) as kss,
                any_value(k.category) as category,
                max(e.date)
            from
                dj_kneequery_examination as e
                left outer join dj_kneequery_kss as k on k.examination_id = e.id
                left outer join dj_kneequery_womac as w on w.examination_id = e.id
            group by
                pid
        ) as e on p.id = pid
    order by
        category desc,
        kss,
        age,
        womac,
        name
        '''

get_patients = '''
    select
        p.id as id,
        p.name as name,
        age,
        address,
        number,
        womac,
        kss,
        jk,
        category,
        max(o.date),
        any_value(o.id) as operated
    from
        dj_kneequery_patient as p
        left outer join dj_kneequery_operation as o on p.id = o.patient_id
    left join
    (
        select
            e.patient_id as pid,
            any_value(w.score) as womac,
            any_value(k.score) as kss,
            any_value(j.score) as jk,
            any_value(k.category) as category,
            max(e.date)
        from
            dj_kneequery_examination as e
            left outer join dj_kneequery_kss as k on k.examination_id = e.id
            left outer join dj_kneequery_womac as w on w.examination_id = e.id
            left outer join dj_kneequery_jk as j on j.examination_id = e.id
        group by pid
    ) as e on p.id = pid
    group by p.id
    '''

search_patient = '''
    select id, name
    from dj_kneequery_patient as p
    where p.name like %s
'''
