from django.apps import AppConfig


class DjKneequeryConfig(AppConfig):
    name = 'dj_kneequery'
